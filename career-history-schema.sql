CREATE TABLE `company` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `states` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_niu8sfil2gxywcru9ah3r4ec5` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `person` (
  `id` int(11) NOT NULL,
  `age` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `state` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `personjob` (
  `id` int(11) NOT NULL,
  `enddate` datetime(6) DEFAULT NULL,
  `startdate` datetime(6) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `companyid` int(11) NOT NULL,
  `personid` int(11) NOT NULL,
  `positionid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK476xlvdx9cn3sfagjt286ia9g` (`companyid`),
  KEY `FKjh8imthnjotdlcm19qaktr8bb` (`personid`),
  KEY `FKrx315det73vdp50q2yuqi4cln` (`positionid`),
  CONSTRAINT `FK476xlvdx9cn3sfagjt286ia9g` FOREIGN KEY (`companyid`) REFERENCES `company` (`id`),
  CONSTRAINT `FKjh8imthnjotdlcm19qaktr8bb` FOREIGN KEY (`personid`) REFERENCES `person` (`id`),
  CONSTRAINT `FKrx315det73vdp50q2yuqi4cln` FOREIGN KEY (`positionid`) REFERENCES `position` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `position` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_qe48lxuex3swuovou3giy8qpk` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
