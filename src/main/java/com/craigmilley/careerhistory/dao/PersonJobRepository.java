package com.craigmilley.careerhistory.dao;

import com.craigmilley.careerhistory.model.PersonJob;
import org.springframework.data.repository.CrudRepository;

/**
 * Interface for CRUD operations on PersonJob objects.  Interface implementation provided by framework at runtime.
 *
 * @author cmilley
 */
public interface PersonJobRepository extends CrudRepository<PersonJob, Integer>  {
}
