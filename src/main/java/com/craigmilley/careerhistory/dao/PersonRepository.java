package com.craigmilley.careerhistory.dao;

import com.craigmilley.careerhistory.model.Person;
import org.springframework.data.repository.CrudRepository;

/**
 * Interface for CRUD operations on Person objects.  Interface implementation provided by framework at runtime.
 *
 * @author cmilley
 */
public interface PersonRepository extends CrudRepository<Person, Integer>  {
}
