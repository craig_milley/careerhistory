package com.craigmilley.careerhistory.dao;

import com.craigmilley.careerhistory.model.Position;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * Interface for CRUD operations on Position objects.  Interface implementation provided by framework at runtime.
 *
 * @author cmilley
 */
public interface PositionRepository extends CrudRepository<Position, Integer> {
    Optional<Position> findByName(String name);
}
