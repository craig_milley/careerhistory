package com.craigmilley.careerhistory.dao;

import com.craigmilley.careerhistory.model.Company;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * Interface for CRUD operations on Company objects.  Interface implementation provided by framework at runtime.
 *
 * @author cmilley
 */
public interface CompanyRepository extends CrudRepository<Company, Integer> {

    Optional<Company> findByName(String name);
}
