package com.craigmilley.careerhistory.controller;

import com.craigmilley.careerhistory.dao.PositionRepository;
import com.craigmilley.careerhistory.model.Position;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for simple CRUD operations on Position objects.  Endpoints are: Endpoints are:
 *
 * <ul>
 * <li>GET      /career-history/positions</li>
 * <li>POST     /career-history/positions</li>
 * <li>GET      /career-history/positions/{position_id}</li>
 * <li>DELETE   /career-history/positions/{position_id}</li>
 * </ul>
 * <p/>
 * Note:  This was not explicitly part of exercise but facilitates debugging.
 *
 * @author cmilley
 */
@RestController
@RequestMapping(path = "/career-history/positions")
public class PositionsController {

    @Autowired
    private PositionRepository positionRepository;

    /**
     * Get all positions in the system.
     * <p>
     * TODO:  Provide paging.
     *
     * @return iterable of all the Positions in the system.
     */
    @GetMapping(path = "")
    public Iterable<Position> findAllPositions() {
        return positionRepository.findAll();
    }

    /**
     * Find a single position in the system by ID.  Throw an exception which maps to an HTTP 404 code if not found.
     *
     * @param positionID the ID
     * @return the found object.
     */
    @GetMapping(path = "/{position_id}")
    public Position findPosition(@PathVariable("position_id") int positionID) {
        return positionRepository.findById(positionID).orElseThrow(() ->
                new ResourceNotFoundException(String.format("Position [%d] not found.", positionID)));
    }

    /**
     * Add a position by name
     *
     * @param name position name
     * @return the create object.
     */
    @PostMapping(path = "")
    @Operation(summary = "Add a position by name.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Position added", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Position.class)) }),
            @ApiResponse(responseCode = "409", description = "A position with this name already exists.")
    })
    public Position addPosition(@RequestParam String name) {
        if (positionRepository.findByName(name).isPresent()) {
            throw new ResourceAlreadyExistsException(String.format("Position with name [%s] already exists.", name));
        }
        return positionRepository.save(new Position(name));
    }

    /**
     * Delete a position by ID.
     *
     * @param positionID ID of the position
     */
    @DeleteMapping(path = "/{position_id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    @Operation(summary = "Delete a position")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Position successfully deleted."),
            @ApiResponse(responseCode = "404", description = "A position with this ID could not be found.")
    }
    )
    public void deletePosition(@PathVariable("position_id") int positionID) {
        Position position = positionRepository.findById(positionID).orElseThrow(() ->
                new ResourceNotFoundException(String.format("Position [%d] not found.", positionID)));
        positionRepository.delete(position);
    }
}
