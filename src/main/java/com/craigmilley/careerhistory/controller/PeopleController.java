package com.craigmilley.careerhistory.controller;

import com.craigmilley.careerhistory.dao.CompanyRepository;
import com.craigmilley.careerhistory.dao.PersonRepository;
import com.craigmilley.careerhistory.dao.PositionRepository;
import com.craigmilley.careerhistory.model.Company;
import com.craigmilley.careerhistory.model.Person;
import com.craigmilley.careerhistory.model.PersonJob;
import com.craigmilley.careerhistory.model.Position;
import com.craigmilley.careerhistory.model.State;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * REST controller for simple CRUD operations on Person objects and also the job history associated with the person.
 * Endpoints are:
 *
 * <ul>
 * <li>GET      /career-history/people</li>
 * <li>POST     /career-history/people  (first_name={first_name}&last_name={last_name}&age={age}&state={state_code})</li>
 * <li>GET      /career-history/people/{person_id}</li>
 * <li>DELETE   /career-history/people/{person_id}</li>
 * <li>GET      /career-history/people/{person_id}/jobs</li>
 * <li>POST     /career-history/people/{person_id}/jobs/ (company_id={company_id}&position_id={position_id}&state={state}&start_date={start_date}&end_date={end_date})</li>
 * </ul>
 *
 * @author cmilley
 */
@RestController
@RequestMapping(path = "/career-history/people")
public class PeopleController {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PositionRepository positionRepository;

    @Autowired
    private CompanyRepository companyRepository;

    /**
     * Get all people in the system.
     * <p>
     * TODO:  Provide paging.
     *
     * @return iterable of all the Person objects in the system.
     */
    @GetMapping(path = "")
    public Iterable<Person> findAllPeople() {
        return personRepository.findAll();
    }

    /**
     * Find a single person in the system by ID.  Throw an exception which maps to an HTTP 404 code if not found.
     *
     * @param personID the Person ID
     * @return the found object.
     */
    @GetMapping(path = "/{person_id}")
    public Person findPerson(@PathVariable("person_id") int personID) {
        return personRepository.findById(personID).orElseThrow(() ->
                new ResourceNotFoundException(String.format("Person [%d] not found.", personID)));
    }

    /**
     * Delete a single person by ID.
     *
     * @param personID ID of the person to delete.
     */
    @DeleteMapping(path = "/{person_id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deletePerson(@PathVariable("person_id") int personID) {
        Person person = personRepository.findById(personID).orElseThrow(() ->
                new ResourceNotFoundException(String.format("Person [%d] not found.", personID)));
        personRepository.delete(person);
    }

    /**
     * Add a person by first name, last name, age, and state.
     *
     * @param firstName person first name
     * @param lastName  person last name
     * @param age       person current age.
     * @param stateCode 2 digit state code
     * @return the created object
     */
    @PostMapping(path = "")
    public Person addPerson(@RequestParam("first_name") String firstName,
                     @RequestParam("last_name") String lastName,
                     @RequestParam("age") int age,
                     @RequestParam("state") String stateCode) {
        return personRepository.save(new Person(firstName, lastName, age, State.valueOfAbbreviation(stateCode)));
    }

    /**
     * Find a single person in the system by ID and return his job history. Throw an exception which maps to an HTTP 404
     * code if person is not found.  This is the "Career info" API required by the exercise.
     *
     * @param personID the Person ID
     * @return the found object.
     */
    @GetMapping(path = "/{person_id}/jobs")
    public List<PersonJob> getPersonJobHistory(@PathVariable("person_id") int personID) {
        Person person = personRepository.findById(personID).orElseThrow(() ->
                new ResourceNotFoundException(String.format("Person [%d] not found.", personID)));
        return person.getJobHistory();
    }

    /**
     * Add a job to a specified person's job history
     *
     * @param personID   the person specified as path variable
     * @param companyID  the company ID
     * @param positionID the position ID
     * @param stateCode  the 2 digit state code
     * @param startDate  the start date of the person
     * @param endDate    the end date of the person at this job (optional)
     * @return the updated Person object with the new job.
     */
    @PostMapping(path = "/{person_id}/jobs")
    @Operation(summary = "Add a job to the career history of a Person.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Person job added", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Person.class)) }),
            @ApiResponse(responseCode = "404", description = "Person ID, company ID, position ID, or state is not valid.")
    })
    public Person addPersonJob(@PathVariable("person_id") int personID,
                        @RequestParam("company_id") int companyID,
                        @RequestParam("position_id") int positionID,
                        @RequestParam("state") String stateCode,
                        @RequestParam(name = "start_date") Date startDate,
                        @RequestParam(name = "end_date", required = false) Date endDate) {

        Person person = personRepository.findById(personID).orElseThrow(() ->
                new ResourceNotFoundException(String.format("Person [%d] not found.", personID)));

        Company company = companyRepository.findById(companyID).orElseThrow(() ->
                new ResourceNotFoundException(String.format("Company [%d] not found.", companyID)));

        Position position = positionRepository.findById(positionID).orElseThrow(() ->
                new ResourceNotFoundException(String.format("Position [%d] not found.", positionID)));

        State state = State.valueOfAbbreviation(stateCode);
        if (state.equals(State.UNKNOWN)) {
            throw new ResourceNotFoundException(String.format("State [%s] not found.", stateCode));
        }

        person.getJobHistory().add(new PersonJob(person, company, position, state, startDate, endDate));
        return personRepository.save(person);
    }
}
