package com.craigmilley.careerhistory.controller;

import com.craigmilley.careerhistory.DataGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for dynamically generating data. Endpoints are:
 *
 * <ul>
 * <li>GET      /career-history/data/generate</li>
 * <li>GET      /career-history/data/clean</li>
 * </ul>
 *
 * @author cmilley
 */
@RestController
@RequestMapping(path = "/career-history/data")
public class DataController {

    @Autowired
    private DataGenerator dataGenerator;

    /**
     * Generate the test data.
     * <p/>
     * TODO:  This should be asynch since it can take several seconds.  Return a token which can be queried for task
     * status.
     *
     * @return statistics objects representing the result of the generation.
     */
    @GetMapping(path = "/generate")
    public DataGenerator.GenerateResults generateData() {
        return dataGenerator.generateAll(true);
    }

    /**
     * Delete all test data in the system.
     * <p/>
     * TODO:  This should be asynch since it can take several seconds.  Return a token which can be queried for task
     * status.
     *
     * @return
     */
    @GetMapping(path = "/clean")
    public String cleanData() {
        dataGenerator.cleanAll();
        return "DATA CLEANED";
    }
}
