package com.craigmilley.careerhistory.controller;

import com.craigmilley.careerhistory.dao.CompanyRepository;
import com.craigmilley.careerhistory.model.Company;
import com.craigmilley.careerhistory.model.State;
import com.craigmilley.careerhistory.query.CompanyRetentionQuery;
import com.craigmilley.careerhistory.query.MostTenuredQuery;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * REST controller for simple CRUD operations on Company objects.  Also contains some ad-hoc queries on companies and
 * company employees.  Endpoints are:
 *
 * <ul>
 * <li>GET      /career-history/companies</li>
 * <li>POST     /career-history/companies (name={company_name}&state={code})</li>
 * <li>GET      /career-history/companies/retention</li>
 * <li>GET      /career-history/companies/{company_id}</li>
 * <li>GET      /career-history/companies/{company_id}/most_tenured</li>
 * </ul>
 *
 * @author cmilley
 */
@RestController
@RequestMapping(path = "/career-history/companies")
public class CompaniesController {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private CompanyRetentionQuery companyRetentionQuery;

    @Autowired
    private MostTenuredQuery mostTenuredQuery;

    /**
     * Get all companies in the system.
     *
     * @return iterable of all the Company objects in the system.
     */
    @GetMapping(path = "")
    public Iterable<Company> findAllCompanies() {
        return companyRepository.findAll();
    }

    /**
     * Find a single company in the system by ID.  Throw an exception which maps to an HTTP 404 code if not found.
     *
     * @param companyID the company ID
     * @return the found object.
     */
    @GetMapping(path = "/{company_id}")
    public Company findCompany(@PathVariable("company_id") int companyID) {
        return companyRepository.findById(companyID).orElseThrow(() ->
                new ResourceNotFoundException(String.format("Company [%d] not found.", companyID)));
    }

    /**
     * Add a company by name and state codes.
     *
     * @param name       company name
     * @param stateCodes the 2 digit state codes where the company has offices.  If any are invalid, will be replaced
     *                   with {@link State#UNKNOWN}.
     * @return the created object.
     */
    @PostMapping(path = "")
    public Company addCompany(@RequestParam String name,
                       @RequestParam("state") String[] stateCodes) {

        // Validate company does not already exist.
        if (companyRepository.findByName(name).isPresent()) {
            throw new ResourceAlreadyExistsException(String.format("Company with name [%s] already exists.", name));
        }

        // Coerce state params into required structure.
        ArrayList<State> states = new ArrayList<>();
        for (String stateCode : stateCodes) {
            states.add(State.valueOfAbbreviation(stateCode));
        }

        // Insert into repo.
        return companyRepository.save(new Company(name, states));
    }

    /**
     * Delete a company by ID.
     *
     * @param companyID company ID.
     */
    @DeleteMapping(path = "/{company_id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable("company_id") int companyID) {
        Company company = companyRepository.findById(companyID).orElseThrow(() ->
                new ResourceNotFoundException(String.format("Company [%d] not found.", companyID)));
        companyRepository.delete(company);
    }

    /**
     * Get the average all-time retention for all companies in the system ordered by retention descending. Retention is
     * measured in years with decimal precision of 1.
     *
     * @return list of objects with company and retention properties that can be easily formatted to desired JSON.
     */
    @GetMapping(path = "/retention")
    @Operation(description = "Get the average employee retention in years for each company in the system.  Retention includes past and current employees")
    public List<CompanyRetentionQuery.CompanyRetention> getCompanyRetention() {
        return companyRetentionQuery.getCompanyRetention();
    }

    /**
     * Get the longest tenured employees (current or past) at the specified company ordered
     *
     * @param companyID the company to retrieve employee info for.
     * @return list of employee objects with name, age, and tenure properties which can be automatically formatted as
     * JSON.
     */
    @GetMapping(path = "/{company_id}/most_tenured")
    @Operation(description = "Get the longest tenured employees (current or past) for the specified company.  Query only returns employees whose tenure exceeds the company average.")
    public MostTenuredQuery.MostTenured getMostTenured(@PathVariable("company_id") int companyID) {
        // check existence of company
        Company company = companyRepository.findById(companyID).orElseThrow(() ->
                new ResourceNotFoundException(String.format("Company [%d] not found.", companyID)));
        return mostTenuredQuery.getMostTenured(company);
    }
}
