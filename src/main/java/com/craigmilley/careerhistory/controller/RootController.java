package com.craigmilley.careerhistory.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(path = "/")
public class RootController {

    @GetMapping(path = "")
    @Operation(description = "Redirect to API documentation for career-history app.")
    public ModelAndView root() {
        return new ModelAndView("redirect:/swagger-ui.html");
    }

    @GetMapping(path = "/career-history")
    @Operation(description = "Redirect to API documentation for career-history app.")
    public ModelAndView careerHistoryRoot() {
        return new ModelAndView("redirect:/swagger-ui.html");
    }

}
