package com.craigmilley.careerhistory.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * JPA entity class representing a job position (e.g. "Bus Driver").  Position consists of only an ID and a name.
 *
 * @author cmilley
 */
@Entity
public class Position {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(unique=true)
    private String name;

    /**
     * Static list of positions provided as part of coding exercise.  List is unmodifiable.
     */
    public final static List<String> DEFAULT_POSITION_NAMES;

    static {
        DEFAULT_POSITION_NAMES = Collections.unmodifiableList(Arrays.asList(
                "Back-end Software Engineer", "Front-end Software Engineer", "Manager",
                "CEO", "Accountant", "Rockstar", "Personal Trainer", "Physical Therapist", "Brand Manager", "Human Resources", "Pilot",
                "Flight Attendant", "Bus Driver", "Teacher", "Principal",
                "Event Planner", "Security Advisor", "Booking Specialist", "Account Executive", "Counselor"));
    }

    /**
     * Empty constructor typically needed for POJO.  Do not explicitly use.
     */
    public Position() {

    }

    public Position(String name) {
        this.name = name;
    }

    /**
     * Get the automatic ID stamp provided by the database.
     *
     * @return the position ID.
     */
    public Integer getID() {
        return id;
    }

    /**
     * Get the position name (e.g. "Bus Driver")
     *
     * @return the position name.
     */
    public String getName() {
        return name;
    }
}
