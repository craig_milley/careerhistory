package com.craigmilley.careerhistory.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * JPA entity class representing a company.  Company consists of only a name and one more states in the US where it
 * has a location in this simplified model.
 *
 * @author cmilley
 */
@Entity
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(unique=true)
    private String name;

    private String states;

    /**
     * Static list of companies provided as part of coding exercise.  List is unmodifiable.
     */
    public static final List<Company> DEFAULT_COMPANIES;

    static {
        DEFAULT_COMPANIES = Collections.unmodifiableList(Arrays.asList(
                new Company("Northeastern University", Arrays.asList("MA", "NC")),
                new Company("University of Miami", Collections.singletonList("FL")),
                new Company("NH Learning Solutions", Collections.singletonList("NH")),
                new Company("EverTrue", Collections.singletonList("MA")),
                new Company("Google", Arrays.asList("CA", "MA", "KY")),
                new Company("TripAdvisor", Arrays.asList("MA", "NV", "OH")),
                new Company("Microsoft", Collections.singletonList("WA")),
                new Company("GoDaddy", Arrays.asList("AZ", "NV", "DC")),
                new Company("SXSW", Collections.singletonList("TX")),
                new Company("HBO", Arrays.asList("NY", "RI")),
                new Company("Fender", Arrays.asList("AZ", "CA", "TN")),
                new Company("Alaskan Airlines", Collections.singletonList("WA")),
                new Company("Southern Living Magazine", Arrays.asList("AL", "TX", "LA", "GA", "MO")),
                new Company("Goldman Sachs", Collections.singletonList("NY")),
                new Company("Planet Fitness", Arrays.asList("NH", "ME")),
                new Company("Gold’s Gym", Arrays.asList("TX", "GA", "KY")),
                new Company("Arnold Strongman Classic", Arrays.asList("OH", "WV", "MT"))));
    }

    /**
     * Empty constructor typically needed for POJO.  Do not explicitly use.
     */
    public Company() {

    }

    /**
     * Helper constructor which converts a set of state codes to corresponding enum values.
     *
     * @param name company name
     * @param stateCodes list of 2-digit state codes where company has locations, allowed to be empty but not null
     */
    public Company(String name, Collection<String> stateCodes) {
        this(name, stateCodes.stream().distinct().map(code -> State.valueOfAbbreviation(code)).collect(Collectors.toList()));
    }

    /**
     * Construct a company from a name and a set of states where it is located.  Note that duplicate states will be
     * filtered out and invalid states will be replaced with {@link State#UNKNOWN}.
     *
     * @param name company name
     * @param states list of 2-digit state codes where company has locations, allowed to be empty but not null
     */
    public Company(String name, List<State> states) {
        this.name = name;
        this.states = toString(states);
    }

    /**
     * Get the automatic ID stamp provided by the database.
     *
     * @return the company ID.
     */
    public int getID() {
        return id;
    }

    /**
     * Get the company name.
     *
     * @return the company name.
     */
    public String getName() {
        return name;
    }

    /**
     * Get the list of states where this company has a location.
     *
     * @return list of states, may be empty but never null.
     */
    public List<State> getStates() {
        String[] stateCodes = states.split(",");
        return Arrays.asList(stateCodes).stream()
                .map(code -> State.valueOfAbbreviation(code))
                .collect(Collectors.toList());

    }

    /**
     * Convert a list of state enum values to a single comma-separated string of states codes for simpler
     * storing in the database.
     *
     * @param states list of states
     * @return comma-separated list of state codes.
     */
    private String toString(List<State> states) {
        List<String> stateCodes = states.stream()
                .distinct()
                .filter(state -> !state.equals(State.UNKNOWN))
                .map(state -> state.getAbbreviation())
                .collect(Collectors.toList());
        return String.join(",", stateCodes);
    }
}