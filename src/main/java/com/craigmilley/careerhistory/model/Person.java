package com.craigmilley.careerhistory.model;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;

import java.util.List;

/**
 * JPA entity class representing a person.  Person consists of a first name, last name, current age, current state of
 * residence and job history in this simplified model.
 *
 * @author cmilley
 */
@Entity
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    // Specify a column name here, otherwise property firstName would map to first_name.
    @Column(name = "firstname", nullable=false)
    private String firstName;

    // Specify a column name here, otherwise property lastName would map to last_name.
    @Column(name = "lastname", nullable=false)
    private String lastName;

    @Column(name = "age", nullable=false)
    private Integer age;

    private State state;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER,
            mappedBy = "person")
    @OrderBy("startDate desc") // ensure jobs are ordered descending
    private List<PersonJob> jobHistory;

    /**
     * Empty constructor typically needed for POJO.  Do not explicitly use.
     */
    public Person() {

    }

    /**
     * Construct a person.  Person is immutable after creation except for job history.
     *
     * @param firstName
     * @param lastName
     * @param age
     * @param state
     */
    public Person(String firstName, String lastName, int age, State state) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.state = state;
    }

    /**
     * Get the automatic ID stamp provided by the database.
     *
     * @return the person ID.
     */
    public int getID() {
        return id;
    }

    /**
     * Get the first name.
     *
     * @return the first name.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Get the last name.
     *
     * @return the last name.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Get the current age.
     *
     * @return the current age.
     */
    public int getAge() {
        return age;
    }

    /**
     * Get the state where person lives.
     *
     * @return the state.
     */
    public State getState() {
        return state;
    }

    /**
     * Get the job history of this person.
     *
     * TODO:  Ensure sorted by reverse start_date descending.
     * @return the job history of this person.
     */
    public List<PersonJob> getJobHistory() {
        return jobHistory;
    }

    /**
     * Set the job history of this person.
     *
     * @param jobHistory
     */
    public void setJobHistory(List<PersonJob> jobHistory) {
        this.jobHistory = jobHistory;
    }
}
