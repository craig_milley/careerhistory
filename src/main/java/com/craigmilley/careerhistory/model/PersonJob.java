package com.craigmilley.careerhistory.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

import java.util.Date;

/**
 * JPA entity class representing a person's time of employment at a job. PersonJob consists of a person, company,
 * position, state where the job was located, start date, and end date.
 *
 * @author cmilley
 */
@Entity
@Table(name = "PERSONJOB") // Specify a table name or  person_job will be assumed.
public class PersonJob {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    //  Use  all eager fetches here since object graph is small.

    // Many-to-one:  Person may have held many PersonJob.
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "personid", nullable = false)
    private Person person;

    // One-to-one:  each PersonJob is at one Company.
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "companyid", nullable = false)
    private Company company;

    //  One-to-one:  each PersonJob is one specific Position
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "positionid", nullable = false)
    private Position position;

    @Column(name = "state")
    private String state;

    // Specify a column name here or start_date will be assumed.
    @Column(name = "startdate")
    private Date startDate;

    // Specify a column name here or end_date will be assumed.
    @Column(name = "enddate")
    private Date endDate;

    /**
     * Empty constructor typically needed for POJO.  Do not explicitly use.
     */
    public PersonJob() {

    }

    /**
     * Construct a PersonJob representing a person's term of employment at a company.
     *
     * @param person
     * @param company
     * @param position
     * @param state
     * @param startDate
     * @param endDate
     */
    public PersonJob(Person person, Company company, Position position, State state, Date startDate, Date endDate) {
        this.person = person;
        this.company = company;
        this.position = position;
        this.state = state.getAbbreviation();
        this.startDate = startDate;
        this.endDate = endDate;
    }

    /**
     * Get the automatic ID stamp provided by the database.
     *
     * @return the ID of the entity.
     */
    //@JsonIgnore
    public int getID() {
        return id;
    }

    /**
     * Get the person who held the job.
     *
     * @return the Person holding the job.
     */
    @JsonIgnore // to avoid infinite loop in bidirectional relationship
    public Person getPerson() {
        return person;
    }

    /**
     * Get the company where the person worked.
     *
     * @return the company where the person worked.
     */
    @JsonUnwrapped(prefix = "company_")
    public Company getCompany() {
        return company;
    }

    /**
     * Get the most recent position of the person when he worked at the company.
     *
     * @return the person's position held
     */
    @JsonUnwrapped(prefix = "position_")
    public Position getPosition() {
        return position;
    }

    /**
     * Get the start date when the person started working at the company (typically not null).
     *
     * @return the person's start date.
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Get the end date when the person stopped working at the company.  May  be null if person still works there.
     *
     * @return the person's end date, or null if he still work there.
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    public Date getEndDate() {
        return endDate;
    }

    /**
     * The state where the job is located.
     * @return
     */
    public State getState() {
        return State.valueOfAbbreviation(state);
    }
}
