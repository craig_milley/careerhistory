package com.craigmilley.careerhistory;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {
    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI().info(apiInfo());
    }

    private Info apiInfo() {
        return new Info()
                .title("Career History")
                .description("API for managing a career history - coding exercise")
                .version("1.0")
                .contact(apiContact())
                .license(new License().name("Apache 2.0").url("http://springdoc.org"));
    }

    private Contact apiContact() {
        return new Contact()
                .name("Craig Milley")
                .email("craig.milley@gmail.com")
                .url("https://bitbucket.com/craig_milley");
    }
}