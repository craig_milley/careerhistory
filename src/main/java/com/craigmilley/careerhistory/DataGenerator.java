package com.craigmilley.careerhistory;

import com.craigmilley.careerhistory.dao.CompanyRepository;
import com.craigmilley.careerhistory.dao.PersonJobRepository;
import com.craigmilley.careerhistory.dao.PersonRepository;
import com.craigmilley.careerhistory.dao.PositionRepository;
import com.craigmilley.careerhistory.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Class for generating test data.
 *
 * @author cmilley
 */
@Component // mark as Component so that framework automatically instantiates an object and adds to Spring context.
public class DataGenerator {

    public static final int MIN_WORK_AGE = 18;
    public static final int MAX_JOBS = 3;
    public static final String PERSON_CSV_FILE = "person-data.csv";

    @Autowired
    private PositionRepository positionRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PersonJobRepository personJobRepository;

    private Random randomizer = new Random(System.currentTimeMillis());

    /**
     * Helper class which shows the results of data generation in a format approriate for displaying as JSO.
     */
    public static class GenerateResults {
        public long companies = 0;
        public long positions = 0;
        public long people = 0;
        public long jobs = 0;
    }

    /**
     * Generate a set of Company, Position, Person, and PersonJob objects.  Test companies are defined in Java code.
     * Test positions are also defined in Java code.  Test people are defined in CSV file provided as part of the
     * exercise.  Jobs are created randomly.  Each person over the age of 18 is assigned 0-3 jobs.  Each job is at a
     * company chosen randomly from set of companies.  Same thing with positions.  No person has a job at the same
     * company twice.  Job start dates are chosen randomly from the year when the person turned 18 till the present. All
     * people are assumed to be still employed at their most recent job (no retirement concept!).  The state in which
     * the job is held is chosen randomly from the states where the company has offices, and is not correlated with
     * person's home state in any way.
     * <p/>
     * Note:  Insertion of Person and PersonJob objects is done in batches, since this can be time consuming.
     *
     * @param cleanFirst flag indicating whether to delete all existing data from tables prior to generating test data.
     * @return statistics object representing how much of each type of object was created.
     */
    public GenerateResults generateAll(boolean cleanFirst) {

        if (cleanFirst) {
            cleanAll();
        }

        GenerateResults ret = new GenerateResults();
        ret.companies = generateCompanies();
        ret.positions = generatePositions();
        ret.people = generatePeople();
        ret.jobs = generatePeopleJobs();
        return ret;
    }

    public void cleanAll() {
        // deletion order matters here. We do not have cascading delete hooked up.
        deleteAllPersonJobs();
        deleteAllPeople();
        deleteAllCompanies();
        deleteAllPositions();
    }

    private int generateCompanies() {
        AtomicInteger count = new AtomicInteger();
        Company.DEFAULT_COMPANIES.stream()
                .forEach(company -> {
                    companyRepository.save(company);
                    count.incrementAndGet();
                });
        return count.intValue();
    }


    private int generatePositions() {
        AtomicInteger count = new AtomicInteger();
        Position.DEFAULT_POSITION_NAMES.stream()
                .map(name -> new Position(name))
                .forEach(position -> {
                    positionRepository.save(position);
                    count.incrementAndGet();
                });
        return count.intValue();
    }

    private int generatePeople() {

        List<Person> generated = new ArrayList<>();

        try {
            File file = new ClassPathResource(PERSON_CSV_FILE).getFile();
            Files.lines(file.toPath()).skip(1).forEach(line -> {
                String[] a = line.split(",");

                String firstName = a[0];
                String lastName = a[1];
                int age = Integer.parseInt(a[2]);
                State state = State.valueOfAbbreviation(a[3]);

                generated.add(new Person(firstName, lastName, age, state));

            });

            personRepository.saveAll(generated);

        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return generated.size();
    }

    private int generatePeopleJobs() {

        List<Company> allCompanies = new ArrayList<>();
        companyRepository.findAll().forEach(company -> allCompanies.add(company));

        List<Position> allPositions = new ArrayList<>();
        positionRepository.findAll().forEach(position -> allPositions.add(position));

        List<PersonJob> generated = new ArrayList<>();

        for (Person person : personRepository.findAll()) {
            int age = person.getAge();
            if (age <= MIN_WORK_AGE) {
                // assume folks less than 18 have not worked yet
                continue;
            }

            int jobCount = randomizer.nextInt(MAX_JOBS + 1); // 0-3 jobs
            if (jobCount == 0) {
                // nothing more to do
                continue;
            }

            List<Date> dates = randomDates(age, jobCount);

            Set<Company> personCompanies = new HashSet<>();
            for (int i = 0; i < jobCount; i++) {
                // Determine a random company.  Ensure no duplicates.
                Company company;
                do {
                    company = allCompanies.get(randomizer.nextInt(allCompanies.size()));
                }
                while (personCompanies.contains(company));
                personCompanies.add(company);

                // Determine a random position.
                Position position = allPositions.get(randomizer.nextInt(allPositions.size()));

                // Determine start and end date
                Date startDate = dates.get(i);
                Date endDate = null;
                if (i < dates.size() - 1) {
                    endDate = dates.get(i + 1);
                }

                // Determine state at random from list of company states.
                State state = company.getStates().get(randomizer.nextInt(company.getStates().size()));

                generated.add(new PersonJob(person, company, position, state, startDate, endDate));
            }
        }

        personJobRepository.saveAll(generated);
        return generated.size();
    }

    private List<Date> randomDates(int currentAge, int jobCount) {
        int yearsWorking = currentAge - 18;

        Calendar now = Calendar.getInstance();
        long nowTime = now.getTimeInMillis();

        now.add(Calendar.YEAR, -1 * yearsWorking);
        long startWorkingTime = now.getTimeInMillis();

        // Generate random start dates for the job
        List<Date> startDates = new ArrayList<>();
        for (int i = 0; i < jobCount; i++) {
            long number = (long) (randomizer.nextDouble() * (nowTime - startWorkingTime));
            startDates.add(new Date(startWorkingTime + number));
        }
        Collections.sort(startDates);

        return startDates;
    }

    private void deleteAllPositions() {
        positionRepository.deleteAll();
    }

    private void deleteAllCompanies() {
        companyRepository.deleteAll();
    }

    private void deleteAllPeople() {
        personRepository.deleteAll();
    }

    private void deleteAllPersonJobs() {
        personJobRepository.deleteAll();
    }
}
