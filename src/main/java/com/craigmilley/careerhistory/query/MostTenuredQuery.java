package com.craigmilley.careerhistory.query;

import com.craigmilley.careerhistory.model.Company;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
public class MostTenuredQuery {

    /**
     * Native MySQL query to get the employees at a specific company who have a tenure greater than the average for
     * employees at at that company. Sort by tenure descending. Round to 1 decimal point for readability. Query requires
     * 1 parameter: companyID.
     */
    private static final String MOST_TENURED_SQL =
            "select c.name as company, \n" +
                    "       r.avgRetention, \n" +
                    "       concat(p.firstname, ' ', p.lastname) as name, \n" +
                    "       p.age,\n" +
                    "       round(datediff(ifnull(enddate, NOW()), startdate) / 365, 1) as retention\n" +
                    "from   person p, \n" +
                    "       company c, \n" +
                    "       personjob pj,\n" +
                    "       (\n" +
                    "          select round(avg(datediff(ifnull(enddate, NOW()), startdate) / 365), 1) as avgRetention\n" +
                    "          from personjob pj\n" +
                    "          where companyid = ?1\n" +
                    "       ) r\n" +
                    " where pj.personid = p.id\n" +
                    "       and pj.companyid = c.id\n" +
                    "       and c.id = ?1\n" +
                    "       and round(datediff(ifnull(enddate, NOW()), startdate) / 365, 1) >= avgRetention\n" +
                    "order by retention desc";

    @Autowired
    private EntityManager entityManager;

    public static class PersonTenure {
        public final String name;
        public final int age;
        public final BigDecimal tenure;

        PersonTenure(String name, int age, BigDecimal tenure) {
            this.name = name;
            this.age = age;
            this.tenure = tenure;
        }
    }

    public static class MostTenured {
        public final String company;
        public final BigDecimal avgRetention;
        public final List<PersonTenure> mostTenured = new ArrayList<>();

        MostTenured(String company, BigDecimal avgRetention) {
            this.company = company;
            this.avgRetention = avgRetention;
        }
    }

    public MostTenured getMostTenured(Company company) {

        Query q = entityManager.createNativeQuery(MOST_TENURED_SQL);
        q.setParameter(1, company.getID());

        MostTenured ret = null;

        // Execute the query and map the resultset to an object with the JSON structure we seek.
        for (Object o : q.getResultList()) {
            Object[] a = (Object[]) o;
            if (ret == null) {
                String companyName = (String) a[0];
                BigDecimal avgTenure = (BigDecimal) a[1];
                ret = new MostTenured(companyName, avgTenure);
            }
            String name = (String) a[2];
            int age = (int) a[3];
            BigDecimal tenure = (BigDecimal) a[4];
            ret.mostTenured.add(new PersonTenure(name, age, tenure));
        }
        return ret;
    }
}
