package com.craigmilley.careerhistory.query;

import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
public class CompanyRetentionQuery {

    /**
     * Native MySQL query to get the companies which retain employees the longest on average sorted by average
     * descending. Round to 1 decimal point for readability.
     */
    private static final String COMPANY_RETENTION_SQL = "select c.name as company, c.id, round(avg(datediff(ifnull(enddate, NOW()), startdate) / 365), 1) as avgRetention\n" +
            "from personjob pj, company c\n" +
            "where pj.companyid = c.id\n" +
            "group by companyid\n" +
            "order by avgRetention desc";

    @Autowired
    private EntityManager entityManager;


    public static class CompanyRetention {
        public final String company;
        public final int companyID;
        public final BigDecimal avgRetention;

        CompanyRetention(String company, int companyID, BigDecimal avgRetention) {
            this.company = company;
            this.companyID = companyID;
            this.avgRetention = avgRetention;
        }
    }

    public List<CompanyRetention> getCompanyRetention() {
        Query q = entityManager.createNativeQuery(COMPANY_RETENTION_SQL);

        // Execute the query and map the resultset to an object with the JSON structure we seek.
        List<CompanyRetention> ret = new ArrayList<>();
        for (Object o : q.getResultList()) {
            Object[] a = (Object[]) o;
            ret.add(new CompanyRetention((String) a[0], (Integer) a[1], (BigDecimal) a[2]));
        }
        return ret;
    }
}
